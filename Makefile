CFLAGS = -std=gnu99 -Wall -Wextra -O2
PIXEL_WIDTH = 800
PIXEL_HEIGHT = 600
MY_PREFIX = "{0x2001, 0x67c, 0x20a1, 0x1234}"
SAVE_PATH = /tmp/

all : pingxelflut

screen.o : screen_sdl.cpp
	g++ -O3 -c -std=c++11  $(CXXFLAGS) -I/usr/include/SDL -DSCREEN_WIDTH=$(PIXEL_WIDTH) -DSCREEN_HEIGHT=$(PIXEL_HEIGHT) -DSAVE_PATH=\"$(SAVE_PATH)\" -o $@ $^

net.o: net_socket.cpp
	g++ -O3 -c -std=c++11 $(CXXFLAGS) -DNET_PREFIX=${MY_PREFIX} -o $@ $^

pingxelflut: screen.o net.o pingxelflut.cpp
	g++ -O3 -std=c++11 $(CXXFLAGS) -o $@ $^ -lpthread -lSDL

clean :
	@rm -f screen.o net.o pingxelflut
