#include "types.hpp"

#pragma once

namespace net {
  class net_iface{
  public:
    virtual ~net_iface() {};
    virtual types::pixel recv(void) = 0;
  };
}
