#include "types.hpp"
#include "net_iface.hpp"

#include <arpa/inet.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <linux/ip.h>
#include <linux/ipv6.h>
#include <linux/udp.h>
#include <net/if.h>
#include <netinet/ether.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <unistd.h>
#include <cstring>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <string>

#pragma once

namespace net{
#define BUF_SIZ 1024
  static uint8_t buf[BUF_SIZ];
  /* Header structures */
  static struct ether_header *eh = (struct ether_header *)buf;
  static struct iphdr *iph = (struct iphdr *)(buf + sizeof(struct ether_header));
  static struct udphdr *udph = (struct udphdr *)(buf + sizeof(struct iphdr) +
                                          sizeof(struct ether_header));
  static struct ifreq ifopts;
  class net_socket: virtual net_iface{
  public:
    net_socket(const std::string& iface);
    ~net_socket(){};
    types::pixel recv();

  private:
    int sockfd;
    int sockopt;
    ssize_t numbytes;
    std::array<uint16_t, 4> my_prefix;
    struct sockaddr_storage their_addr;
    struct sockaddr_storage our_addr;
  };
}
