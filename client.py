#!/usr/bin/env python2
import socket
from PIL import Image
import argparse
import time

sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--cont", type=bool, default=False)
    parser.add_argument("-b", "--base", type=list, default=["0", "0"])
    parser.add_argument("-t", "--timeout", type=float, default=0)
    parser.add_argument("image")
    return parser.parse_args()

def send_pixel(x,y,r,g,b):
    UDP_IP = "2001:67c:20a1:1234:{x:04x}:{y:04x}:{r:02x}{g:02x}:{b:02x}00".format(x=x,y=y,r=r,g=g,b=b)
    sock.sendto("", (UDP_IP, 4242))

if __name__ == "__main__":
    args = parse_args()
    image = Image.open(args.image)
    rgbimage = image.convert('RGB')
    x_base = int(args.base[0])
    y_base = int(args.base[1])
    while(args.cont):
        if args.timeout:
            time.sleep(args.timeout)
        for x in range(0, image.width):
            for y in range(0, image.height):
                send_pixel(x_base+x, y_base+y, *rgbimage.getpixel((x, y)))
